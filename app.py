from flask import Flask
# Hello World Web Application

# Create application object and enable DEBUG mode
app = Flask(__name__)
app.config["DEBUG"] = True

# Decorators for hello_world()
@app.route("/")
@app.route("/hello")
def hello_world():
    # Print "Hello, World!" by returning the string to the application
    return "Hello, world!?!?!?!"

# Decorators for search()
@app.route("/test/<search_query>")
def search(search_query):
    return search_query

@app.route("/integer/<int:value>")
def int_type(value):
    print(value + 1)
    return "correct"

@app.route("/float/<float:value>")
def float_type(value):
    print(value + 1)
    return "correct"

@app.route("/path/<path:value>")
def path_type(value):
    print(value)
    return "correct"

@app.route("/name/<name>")
def index(name):
    if name.lower() == "michael":
        return "Hello, {}".format(name), 200
    else:
        return "Not Found", 404

# Start the application server using run()
if __name__ == "__main__":
    app.run()
